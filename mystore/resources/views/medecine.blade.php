<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Medecines</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
            
            .card {
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                background-color: #fff;
                transition: 0.3s;
                border-radius: 10px;
                height: 300px;
                width: 200px;
                float : left;
                margin: 10px 0px 0px 10px
            }
            
            .card:hover {
                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
            }

            img {
                border-radius: 0px 5px 0px 5px;
                height : 170px;
                
            }

            .container {
                padding: 2px 16px;
            }
            .grid{
                margin-top : 30px;
                background-color: #e6e6fa;
                height : 75vh;
                width : 150vh;
                border-radius: 5px 5px 5px 5px;
            }

            .product{
                margin:20px , 20px, 20px, 20px;
                background-color: #fff;
                height : 200px;
                width : 150px;
            }
            
            .product-img{
                background-color: #fff;
                height : 100px;
                width : 75px;
                
            }

            .product-name{
                font-weight : bold;
                font-size: 12;
            }

            .product-price{
                font-weight : bold;
            }

            .product-status{
                color: #7ff44b;
                font-weight : bold;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    MEDECINES
                </div>

                <div class="links">
                    <a href="/">Home</a>
                    <a href="/catalog">Catalog</a>
                    <a href="/catalog/med_equip">Medical Equipment</a>
                </div>
                <div class="grid">
                    <a href="/medecines/1">
                        <div class="card">
                            <img src="{{ asset('img/med/01.jpg') }}" alt="Avatar" style="width:100%">
                            <div class="container">
                                <div class=product-name>Azithromycin - 500mg</div>
                                <div class=product-price>Rp 10,000</div>
                                <div class=product-status>In stock</div>
                            </div>
                        </div>
                    </a>
                    
                    <a href="/medecines/2">
                        <div class="card">
                            <img src="{{ asset('img/med/02.jpg') }}" alt="Avatar" style="width:100%">
                            <div class="container">
                                <div class=product-name>Panadol blue</div>
                                <div class=product-price>Rp 12,000</div>
                                <div class=product-status>In stock</div>
                            </div>
                        </div>
                    </a>

                    <a href="/medecines/3">
                        <div class="card">
                            <img src="{{ asset('img/med/03.jpg') }}" alt="Avatar" style="width:100%;">
                            <div class="container">
                                <div class=product-name>Tolak Angin sachet</div>
                                <div class=product-price>Rp 3,500</div>
                                <div class=product-status>In stock</div>
                            </div>
                        </div>
                    </a>
                    

                </div>
            </div>
        </div>
    </body>
</html>
