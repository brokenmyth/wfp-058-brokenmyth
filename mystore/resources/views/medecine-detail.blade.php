<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Medecine detail</title>
        
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
            .product{
                margin-top : 30px;
                background-color: #e6e6fa;
                height : 75vh;
                width : 150vh;
                border-radius: 5px 5px 5px 5px;
            }
            .img{
                float:left;
                border-radius: 10px 10px 10px 10px;
                height : 170px;
                margin: 10px 0 0 10px;
            }
            img{
                height:500px;
                width:100%;
                float:left;
                border-radius: 10px 10px 10px 10px;
            }

            .container{
                float:right;
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                background-color: #fff;
                transition: 0.3s;
                border-radius: 10px;
                height: 500px;
                width: 300px;
                margin: 10px 10px 10px 10px
            }
            .product-name{
                font-weight : bold;
                font-size: 24px;
                margin-top: 10px;
            }

            .product-price{
                font-weight : bold;
            }

            .product-status{
                color: #7ff44b;
                font-weight : bold;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    MEDECINES
                </div>

                <div class="links">
                    <a href="/">Home</a>
                    <a href="/catalog">Catalog</a>
                    <a href="/catalog/medecines">Medecines</a>
                    <a href="/catalog/med_equip">Medical Equipment</a>
                </div>
                <div class="product">
                    @if ($id==1)
                        <div class="img">
                            <img src="{{ asset('img/med/01.jpg') }}" alt="Avatar" style="width:100%">  
                        </div>
                        <div class="container">
                            <div class=product-name>Azithromycin - 500mg</div>
                            <div class=product-price>Rp 10,000</div>
                            <div class=product-status>In stock</div>
                            <div><p> <b>Azithromycin</b> dapat melawan berbagai macam bakteri, termasuk golongan bakteri Streptococcus. 
                                Obat ini sering digunakan untuk mengobati infeksi ringan hingga sedang pada paru-paru, sinus, kulit, dan bagian tubuh lainnya. 
                                Dokter umumnya meresepkan Azithromycin untuk infeksi sinus, komplikasi COPD, atau tonsillitis.</p></div>
                        </div>
                    @elseif ($id==2)
                        <div class="img">
                            <img src="{{ asset('img/med/02.jpg') }}" alt="Avatar" style="width:100%">  
                        </div>
                        <div class="container">
                            <div class=product-name>Panadol blue</div>
                            <div class=product-price>Rp 12,000</div>
                            <div class=product-status>In stock</div>
                            <div><p><b>Panadol Blue 10 Kaplet </b>  Paracetamol 500 mg  Mengobati Demam, Nyeri & Sakit Kepala. Panadol Biru adalah obat analgesik yang berfungsi untuk menurunkan demam dan meredakan nyeri. 
                                Mengandung paracetamol 500 mg, Panadol Biru bekerja dengan cara melepaskan panas sehingga suhu tubuh kembali normal</p></div>
                        </div>
                    @elseif ($id==3)
                        <div class="img">
                            <img src="{{ asset('img/med/03.jpg') }}" alt="Avatar" style="width:100%">  
                        </div>
                        <div class="container">
                            <div class=product-name>Tolak Angin sachet</div>
                            <div class=product-price>Rp 3,500</div>
                            <div class=product-status>In stock</div>
                            <div><p><b>Tolak Angin</b> adalah obat herbal yang berguna untuk meredakan masuk angin, perut mual, 
                            tenggorokan kering dan badan terasa dingin. Tolak Angin dibuat oleh pendiri Sido Muncul pada tahun 1930 yaitu Ibu Rahmat Sulistyo. 
                            Tolak Angin dibuat dari tumbuh-tumbuhan herbal dan madu serta ramuan lainnya.</p></div>
                        </div>
                    @else
                        <div><p>404 PRODUCT NOT FOUND</p></div>
                    @endif
                </div>
                    
            </div>
        </div>
    </body>
</html>
