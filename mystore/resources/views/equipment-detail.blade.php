<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Equipment detail</title>
        
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
            .product{
                margin-top : 30px;
                background-color: #e6e6fa;
                height : 75vh;
                width : 150vh;
                border-radius: 5px 5px 5px 5px;
            }
            .img{
                float:left;
                border-radius: 10px 10px 10px 10px;
                height : 170px;
                margin: 10px 0 0 10px;
            }
            img{
                height:500px;
                width:100%;
                float:left;
                border-radius: 10px 10px 10px 10px;
            }

            .container{
                float:right;
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                background-color: #fff;
                transition: 0.3s;
                border-radius: 10px;
                height: 500px;
                width: 300px;
                margin: 10px 10px 10px 10px
            }
            .product-name{
                font-weight : bold;
                font-size: 24px;
                margin-top: 10px;
            }

            .product-price{
                font-weight : bold;
            }

            .product-status{
                color: #7ff44b;
                font-weight : bold;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    EQUIPMENT
                </div>

                <div class="links">
                    <a href="/">Home</a>
                    <a href="/catalog">Catalog</a>
                    <a href="/catalog/medecines">Medecines</a>
                    <a href="/catalog/med_equip">Medical Equipment</a>
                </div>
                <div class="product">
                    @if ($id==1)
                        <div class="img">
                            <img src="{{ asset('img/eq/01.jpg') }}" alt="Avatar" style="width:100%">  
                        </div>
                        <div class="container">
                            <div class=product-name>ZacUrate series 500DL- Pulse Oximater</div>
                            <div class=product-price>Rp 650,000</div>
                            <div class=product-status>In stock</div>
                            <div><p> <b>Zacurate 500DL Pro Series warna Black Pulse oximeter</b> Alat ukur kadar oksigen dalam darah <br><br>ACCURATE AND RELIABLE 
                            <br>Zacurate Pro Series pulse oximeter accurately determines your SpO2 (blood oxygen saturation levels) within +/- 2% deviation. <br>
                            <br>PRO SERIES -Manufactured according to CE (Europe) and FDA standards for pulse oximeters used by doctors and other health professionals. 
                            The ONLY LED pulse oximeter that can read and display up to 100% for SpO2 value</p></div>
                        </div>
                    @elseif ($id==2)
                        <div class="img">
                            <img src="{{ asset('img/eq/02.jpg') }}" alt="Avatar" style="width:100%">  
                        </div>
                        <div class="container">
                            <div class=product-name>Termometer Digital Flexibel Rossmax TG380</div>
                            <div class=product-price>Rp 78,000</div>
                            <div class=product-status>In stock</div>
                            <div><p><b>Termometer rossmax TG380</b> memiliki desain yang lebih fleksibel sehingga tidak mudah patah dan dapat mengukur suhu badan dengan mengubah 2 skala berbeda °C atau °F.
                            <br>Spesifikasi Produk:<br>• Waktu pengukuran 10 detik<br>• Ujung fleksibel<br>• Alarm petunjuk demam<br>• Waterproof<br>• Auto shut-off<br>• Satuan dapat diubah : °C / °F<br><br>
                            Garansi 5 tahun</p></div>
                        </div>
                    @else
                        <div><p>404 PRODUCT NOT FOUND</p></div>
                    @endif
                </div>
                    
            </div>
        </div>
    </body>
</html>
