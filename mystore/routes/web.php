<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('mydrugstore');
});

Route::get('catalog',function(){
    return view("catalog");
});

Route::get('catalog/medecines',function(){
    return view("medecine");
})->name('medecines');

Route::get('medecines/{medecine_id}',function($medecine_id){
    return view("medecine-detail", ['id'=>$medecine_id]);
});

Route::get('catalog/med_equip',function(){
    return view("equipment");
})->name('equipment');

Route::get('equipment/{equip_id}',function($equip_id){
    return view("equipment-detail",['id'=>$equip_id]);
});



Route::get("foo", function (){
    return "fuuuuiii yoooooooh!!!";
});

Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});

Route::get('user/profile', function () {
//
});

Route::get('user/{name?}', function ($name = 'John') {
    return $name;
});


Route:: get('user/profile', function ($id) {
    //
})->name('profile');

Route:: get('user/{id}/profile', function ($id) {
    //
})->name('profile');
// $url = route('profile'.['id'=>1]);

Route::get('greeting', function () {
    return view('welcome', ['name' => 'Kuma']);
});