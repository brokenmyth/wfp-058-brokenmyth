<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedecinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->id();
            $table->string('Generic_Name');
            $table->string('Form');
            $table->string('Restriction_Formula')->nullable();
            $table->string('Description')->nullable();
            $table->boolean('Faskes_TK1')->default(false);
            $table->boolean('Faskes_TK2')->default(false);
            $table->boolean('Faskes_TK3')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
