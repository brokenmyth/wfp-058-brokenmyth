<?php

use Illuminate\Database\Seeder;

class MedicineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicines')->insert([
            'Generic_Name' => 'FENTANIL',
            'Form' => 'inj 0,05 mg/mL (i.m./i.v.)',
            'Restriction_Formula' => '5 amp/kasus.',
            'Description' => 'inj: Hanya untuk nyeri berat dan harus diberikan oleh tim medis yang dapat melakukan resusitasi.',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 1
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'FENTANIL',
            'Form' => 'patch 12,5 mcg/jam',
            'Restriction_Formula' => '10 patch/bulan.',
            'Description' => 'patch: - Untuk nyeri kronik pada pasien kanker yang tidak terkendali.  - Tidak untuk nyeri akut.',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 1
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'FENTANIL',
            'Form' => 'patch 25 mcg/jam',
            'Restriction_Formula' => '10 patch/bulan.',
            'Description' => 'patch: - Untuk nyeri kronik pada pasien kanker yang tidak terkendali.  - Tidak untuk nyeri akut.',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 1
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'ASAM MEDENAMAT',
            'Form' => 'kaps 250 mg ',
            'Restriction_Formula' => '30 kaps/bulan',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 2
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'ASAM MEDENAMAT',
            'Form' => 'tab 500 mg ',
            'Restriction_Formula' => '30 tab/bulan.',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 2
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'IBUPROFEN',
            'Form' => 'tab 200 mg',
            'Restriction_Formula' => '30 tab/bulan',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 2
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'ALOPURINOL',
            'Description' => 'Tidak diberikan pada saat nyeri akut.',
            'Form' => 'tab 100 mg',
            'Restriction_Formula' => '30 tab/bulan',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 3
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'ALOPURINOL',
            'Description' => 'Tidak diberikan pada saat nyeri akut.',
            'Form' => 'tab 300 mg',
            'Restriction_Formula' => '30 tab/bulan',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 3
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'KOLKISIN',
            'Description' => 'Tidak diberikan pada saat nyeri akut.',
            'Form' => 'tab 100 mg',
            'Restriction_Formula' => '30 tab/bulan',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 3
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'AMITRIPTILIN',
            'Form' => 'tab 25 mg',
            'Restriction_Formula' => '30 tab/bulan',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 4
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'GABAPENTIN',
            'Description' => 'Hanya untuk neuralgia pascaherpes atau nyeri neuropati diabetikum.',
            'Form' => 'kaps 100mg',
            'Restriction_Formula' => '60 kaps/bulan',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 4
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'GABAPENTIN',
            'Description' => 'Hanya untuk neuralgia pascaherpes atau nyeri neuropati diabetikum.',
            'Form' => 'kaps 300mg',
            'Restriction_Formula' => '30 kaps/bulan',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 4
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'BUPIVAKAIN',
            'Form' => 'inj 0,5%',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 5
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'BUPIVAKAIN HEAVY',
            'Description' => 'Khusus untuk analgesia spinal.',
            'Form' => 'inj 0,5% + glukosa 8% ',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 5
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'ETIL KLORIDA',
            'Form' => 'spray 100mL',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 5
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'DEKSMEDETOMIDIB',
            'Description' => 'Untuk sedasi pada pasien di ICU, kraniotomi, bedah jantung dan operasi yang memerlukan waktu pembedahan yang lama.',
            'Form' => 'inj 100 mcg/mL',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 6
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'desfluran',
            'Form' => 'ih',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 6
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'HALOTAN',
            'Description' => 'Tidak boleh digunakan berulang. Tidak untuk pasien dengan gangguan fungsi hati.',
            'Form' => 'ih',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 6
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'ATROPIN',
            'Form' => 'inj 0,25 mg/mL (i.v./s.k.)',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 7
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'DIAZEPAM',
            'Form' => 'inj 5 mg/mL',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 7
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'MIDAZOLAM',
            'Description' => 'Dapat digunakan untuk premedikasi sebelum induksi anestesi dan rumatan selama anestesi umum', 'Form' => 'inj 100 mcg/mL',
            'Restriction_Formula' => '1 mg/jam (24 mg/hari). - Dosis premedikasi: 8 vial/kasus',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 7
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'DEKSAMETASON',
            'Restriction_Formula' => '20 mg/hari',
            'Form' => 'inj 5 mg/mL',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 8
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'DIFENHIDRAMIN',
            'Restriction_Formula' => '30 mg/hari.',
            'Form' => 'inj 10 mg/mL (i.v./i.m.) ',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 8
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'EPINEFRIN (ADRENALIN)',
            'Form' => 'inj 1 mg/mL',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 8
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'ATROPIN',
            'Form' => 'tab 0,5 mg ',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 9
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'ATROPIN',
            'Form' => 'inj 0,25 mg/mL (i.v.)',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 9
        ]);
        DB::table('medicines')->insert([
            'Generic_Name' => 'EFEDRIN',
            'Form' => 'inj 10% ',
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 9
        ]);

        DB::table('medicines')->insert([
            'Generic_Name' => 'MAGNESIUM SULFAT',
            'Form' => 'serb',
            'Faskes_TK1' => true,
            'Faskes_TK2' => true,
            'Faskes_TK3' => true,
            'Category' => 10
        ]);
    }
}
